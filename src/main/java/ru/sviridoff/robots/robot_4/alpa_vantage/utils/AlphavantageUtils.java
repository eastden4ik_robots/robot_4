package ru.sviridoff.robots.robot_4.alpa_vantage.utils;

import com.google.gson.Gson;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.robots.robot_4.alpa_vantage.models.RealCurrencyExchangeRate;

public class AlphavantageUtils {

    private Gson gson = new Gson();
    private Logger logger = new Logger(AlphavantageUtils.class.getName());

    public RealCurrencyExchangeRate getRate(String url, String from, String to, String api) {

        RealCurrencyExchangeRate rate = new RealCurrencyExchangeRate();

        HttpResponse<JsonNode> response = Unirest.get(url)
                .header("accept", "application/json")
                .header("User-Agent", "Robot 4. Анализ рынка. JDK 13.")
                .queryString("function","CURRENCY_EXCHANGE_RATE")
                .queryString("from_currency",from)
                .queryString("to_currency",to)
                .queryString("apikey",api)
                .asJson();

        if (response.isSuccess()) {
            logger.steps(response.getStatusText() + " - " + response.getStatus() + " - " + response.getBody().toPrettyString());
            rate = gson.fromJson(response.getBody().toString(), RealCurrencyExchangeRate.class);
            logger.success("From: " + rate.getCurrRate().getFromCode() + ", To: " + rate.getCurrRate().getToCode());
            logger.success("Rate: " + rate.getCurrRate().getRate());
            logger.success("Time: " + rate.getCurrRate().getLastTime());
        } else {
            logger.error(response.getStatusText() + " - " + response.getStatus());
        }
        return rate;
    }

}
