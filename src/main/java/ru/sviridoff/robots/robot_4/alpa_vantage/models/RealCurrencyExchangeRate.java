package ru.sviridoff.robots.robot_4.alpa_vantage.models;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class RealCurrencyExchangeRate {

    @SerializedName("Realtime Currency Exchange Rate")
    private CurrExchangeRate currRate;

}
