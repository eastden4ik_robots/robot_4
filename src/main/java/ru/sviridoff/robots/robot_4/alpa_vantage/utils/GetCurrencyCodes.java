package ru.sviridoff.robots.robot_4.alpa_vantage.utils;

import au.com.bytecode.opencsv.CSVReader;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.robots.robot_4.alpa_vantage.models.CurrencyCsv;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GetCurrencyCodes {

    private static GetCurrencyCodes getCurrencyCodes = null;
    private static Logger logger = new Logger(GetCurrencyCodes.class.getName());

    private static String fileName;
    private List<CurrencyCsv> list;

    private GetCurrencyCodes(String name) {
        fileName = name;
        list = getCodes();
    }

    public static GetCurrencyCodes getInstance(String fileName) {
        if (getCurrencyCodes == null) getCurrencyCodes = new GetCurrencyCodes(fileName);
        return getCurrencyCodes;
    }

    private List<CurrencyCsv> getCodes() {

        List<CurrencyCsv> list = new ArrayList<>();

        try {
            CSVReader reader = new CSVReader(new FileReader(fileName), ',', '"', 1);
            List<String[]> rows = reader.readAll();
            for (String[] row: rows) {
                CurrencyCsv currencyCsv = new CurrencyCsv();
                currencyCsv.setCurrencyCode(row[0]);
                currencyCsv.setCodeCountry(row[1]);
                list.add(currencyCsv);
            }
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }

        return list;
    }

    public String getCodeByName(String name) {
        String result = null;
        for (CurrencyCsv code: list) {
            if (code.getCodeCountry().toLowerCase().equals(name.toLowerCase())) {
                result = code.getCurrencyCode();
            }
        }
        if (result == null) {
            throw new RuntimeException("Кода такой страны нет: " + name);
        }
        return result;
    }

    public void printCodes() {
        for (CurrencyCsv code: list) {
            logger.warning(code.getCurrencyCode() + " >>> " + code.getCodeCountry());
        }
    }

}
