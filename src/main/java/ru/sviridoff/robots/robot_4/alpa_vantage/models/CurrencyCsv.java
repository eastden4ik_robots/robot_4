package ru.sviridoff.robots.robot_4.alpa_vantage.models;

public class CurrencyCsv {

    private String currencyCode;
    private String codeCountry;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCodeCountry() {
        return codeCountry;
    }

    public void setCodeCountry(String codeCountry) {
        this.codeCountry = codeCountry;
    }
}
