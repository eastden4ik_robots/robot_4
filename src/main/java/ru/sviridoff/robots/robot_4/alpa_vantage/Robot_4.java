package ru.sviridoff.robots.robot_4.alpa_vantage;

import com.google.gson.Gson;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.props.Props;
import ru.sviridoff.robots.robot_4.alpa_vantage.models.RealCurrencyExchangeRate;
import ru.sviridoff.robots.robot_4.alpa_vantage.utils.AlphavantageUtils;
import ru.sviridoff.robots.robot_4.alpa_vantage.utils.GetCurrencyCodes;

public class Robot_4 {

    private static Logger logger = new Logger(Robot_4.class.getName());
    private static Gson gson = new Gson();

    private static AlphavantageUtils utils = new AlphavantageUtils();


    public static void main(String[] args) throws InterruptedException {

        logger.info("Robot 4. Creating analyze of stocks, getting from Alphavantage.co.");

        Props props = new Props("props.properties");
        GetCurrencyCodes codes = GetCurrencyCodes.getInstance(props.getProperty("csv.codes"));

        String url = props.getProperty("url");

        String api = props.getProperty("API_TOKEN");
        String from1 = codes.getCodeByName("united states dollar");
        String from2 = codes.getCodeByName("Euro");
        String from3 = codes.getCodeByName("British Pound Sterling");
        String from4 = codes.getCodeByName("Japanese Yen");
        String from5 = codes.getCodeByName("Cuban Peso");
        String from6 = codes.getCodeByName("Old Russian Ruble");
        String from7 = codes.getCodeByName("United Arab Emirates Dirham");
        String from8 = codes.getCodeByName("Canadian Dollar");
        String from9 = codes.getCodeByName("Cuban Peso");
        String from10 = codes.getCodeByName("Czech Republic Koruna");
        String to = codes.getCodeByName("russian ruble");

        RealCurrencyExchangeRate rate1 = utils.getRate(url, from1, to, api);
        RealCurrencyExchangeRate rate2 = utils.getRate(url, from2, to, api);
        RealCurrencyExchangeRate rate3 = utils.getRate(url, from3, to, api);
        RealCurrencyExchangeRate rate4 = utils.getRate(url, from4, to, api);
        Thread.sleep(60*1000);
        RealCurrencyExchangeRate rate5 = utils.getRate(url, from5, to, api);
        RealCurrencyExchangeRate rate6 = utils.getRate(url, from6, to, api);
        RealCurrencyExchangeRate rate7 = utils.getRate(url, from7, to, api);
        RealCurrencyExchangeRate rate8 = utils.getRate(url, from8, to, api);
        Thread.sleep(60*1000);
        RealCurrencyExchangeRate rate9 = utils.getRate(url, from9, to, api);
        RealCurrencyExchangeRate rate10 = utils.getRate(url, from10, to, api);

        logger.info("Robot ended.");

    }






}
