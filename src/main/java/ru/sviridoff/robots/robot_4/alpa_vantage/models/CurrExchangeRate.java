package ru.sviridoff.robots.robot_4.alpa_vantage.models;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class CurrExchangeRate {

    @SerializedName("1. From_Currency Code")
    private String fromCode;

    @SerializedName("2. From_Currency Name")
    private String fromCountry;

    @SerializedName("3. To_Currency Code")
    private String toCode;

    @SerializedName("4. To_Currency Name")
    private String toCountry;

    @SerializedName("5. Exchange Rate")
    private String rate;

    @SerializedName("6. Last Refreshed")
    private String lastTime;

    @SerializedName("7. Time Zone")
    private String timeZone;

    @SerializedName("8. Bid Price")
    private String bidPrice;

    @SerializedName("9. Ask Price")
    private String askPrice;



//        "": "USD",
//                "": "United States Dollar",
//                "": "EUR",
//                "": "Euro",
//                "": "0.90000000",
//                "": "2020-05-30 23:15:47",
//                "": "UTC",
//                "": "0.90000000",
//                "": "0.90050000"

}
