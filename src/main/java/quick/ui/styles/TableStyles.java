package quick.ui.styles;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class TableStyles {


    public static class CustomRenderer extends DefaultTableCellRenderer
    {
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
        {
            Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            setForeground(Color.RED);
            return c;
        }
    }

    public static class CustomRenderer2 extends DefaultTableCellRenderer
    {
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
        {
            Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            setBackground(Color.YELLOW);
            return c;
        }
    }

    public static class MyColorCellRenderer extends DefaultTableCellRenderer {

        private int rowToColor = -1;

        public MyColorCellRenderer() {
        }

        public void setRowToColor(int row) {
            rowToColor = row;
        }

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

            if (rowToColor!=-1 && row==rowToColor)
                setForeground(Color.RED);
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    }

}
