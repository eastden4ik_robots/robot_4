package quick.obligations;

import quick.models.BondModel;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TableWindow implements TableModel {

    private Utils utils = new Utils();

    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

    private List<BondModel> beans;

    List<Color> rowColours = Arrays.asList(
            Color.RED,
            Color.GREEN,
            Color.CYAN
    );

    public TableWindow(List<BondModel> beans) {
        this.beans = beans;
    }

    public void addTableModelListener(TableModelListener listener) {
        listeners.add(listener);
    }

    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public void setRowColour(int row, Color c) {
        rowColours.set(row, c);
//        fireTableRowsUpdated(row, row);
    }


    public Color getRowColor(int index) {
        return Color.YELLOW;
    }

    public int getColumnCount() {
        return 17;
    }

    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Наименование инструмента";
            case 1:
                return "ISIN";
            case 2:
                return "Дата выплаты купонного дохода";
            case 3:
                return "Погашение";
            case 4:
                return "Длительность купона";
            case 5:
                return "Цена закрытия";
            case 6:
                return "НКД";
            case 7:
                return "Размер купона";
            case 8:
                return "Листинг";
            case 9:
                return "Номинал";
            case 10:
                return "Предложения";
            case 11:
                return "До погашения";
            case 12:
                return "Комиссия брокера и биржи, руб";
            case 13:
                return "Платеж за облигацию, руб";
            case 14:
                return "Количество купонов";
            case 15:
                return "Сумма без налога 13%, руб";
            case 16:
                return "Сумма для погашения";
        }
        return "";
    }

    public int getRowCount() {
        return beans.size();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        BondModel bean = beans.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return bean.getInstrumentName();
            case 1:
                return bean.getIsin();
            case 2:
                return bean.getDatePaymentCouponIncome();
            case 3:
                return bean.getRepayment();
            case 4:
                return bean.getDurationCoupon();
            case 5:
                return bean.getClosingPrice();
            case 6:
                return bean.getNKD();
            case 7:
                return bean.getCouponSize();
            case 8:
                return bean.getListing();
            case 9:
                return bean.getNominal();
            case 10:
                return bean.getProposal();
            case 11:
                return bean.getUntilMaturity();
            case 12:
                return utils.print(bean.getFeeCurrBrokerExchange(), 2) + " ₽";
            case 13:
                return utils.print(bean.getPaymentToBuyBond(), 2) + " ₽";
            case 14:
                return bean.getAmountCouponsPayments();
            case 15:
                return utils.print(bean.getSummOfCouponsWithoutTax13(), 2) + " ₽";
            case 16:
                return Color.YELLOW + utils.print(bean.getSummToRepayment(), 2) +  "₽";
        }
        return "";
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public void removeTableModelListener(TableModelListener listener) {
        listeners.remove(listener);
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {

    }

}
