package quick.obligations;

import quick.excel.Excel;
import quick.excel.ExcelEnum;
import quick.models.BondModel;
import quick.ui.styles.TableStyles;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.props.Props;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.text.TableView;
import java.awt.*;
import java.util.List;

public class Manager extends JFrame {


    private static Logger logger = new Logger(Manager.class.getName());
    private static Utils utils = new Utils();
    private static Props props;

    private static Excel excel;

    public Manager() {
        super("Облигации");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        props = new Props("quick.properties");

        String bondsFile = props.getProperty("bonds");
        excel = Excel.getInstance(bondsFile, ExcelEnum.Bonds);

        List<BondModel> bonds = excel.getBonds();

        utils.getFeeCurOfBondBrokerAndExchange(bonds, 0.07d);
        utils.getPaymentToBuy(bonds);
        utils.getAmountCouponsPayments(bonds);
        utils.getSummOfCouponsWithoutTax13(bonds);
        utils.getSummToRepayment(bonds);


        TableWindow model = new TableWindow(bonds);
        model.setRowColour(2,Color.YELLOW);
        JTable table = new JTable(model);


        TableColumn column = table.getColumnModel().getColumn(16);
        column.setCellRenderer(new TableStyles.CustomRenderer());



        getContentPane().add(new JScrollPane(table));

        setPreferredSize(new Dimension(1920, 1080));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }


    public static void main(String[] args) {


        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame.setDefaultLookAndFeelDecorated(true);
                new Manager();
            }
        });



    }





}
