package quick.obligations;

import quick.models.BondModel;
import ru.sviridoff.framework.console.ConsoleColors;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Utils {


    public void print(List<BondModel> bonds) {

        TableList table = new TableList(17,
                "Наименование\nинструмента",
                "ISIN",
                "Дата выплаты купонного дохода",
                "Погашение",
                "Длительность купона",
                "Цена закрытия",
                "НКД",
                "Размер купона",
                "Листинг",
                "Номинал",
                "Предложение",
                "До погашения",
                "Комиссия брокера и биржи, руб.",
                "Стоимость покупки облиги",
                "Количество купонов",
                "Сумма купонов без 13%",
                "Сумма погашения", "", "").align(0, TableList.EnumAlignment.CENTER).withUnicode(true);

        bonds.forEach(element -> {
            table.addRow(
                    element.getInstrumentName(),
                    element.getIsin(),
                    element.getDatePaymentCouponIncome(),
                    element.getRepayment(),
                    Long.toString(element.getDurationCoupon()),
                    Double.toString(element.getClosingPrice()),
                    Double.toString(element.getNKD()),
                    Double.toString(element.getCouponSize()),
                    Long.toString(element.getListing()),
                    Double.toString(element.getNominal()),
                    Double.toString(element.getProposal()),
                    Integer.toString(element.getUntilMaturity()),
                    element.getFeeCurrBrokerExchange() != 0? print(element.getFeeCurrBrokerExchange(), 2): "",
                    (element.getPaymentToBuyBond() != 0? print(element.getPaymentToBuyBond(), 2): ""),
                    Integer.toString(element.getAmountCouponsPayments()),
                    (element.getSummOfCouponsWithoutTax13() != 0? print(element.getSummOfCouponsWithoutTax13(), 2): ""),
                    (element.getSummToRepayment() != 0? print(element.getSummToRepayment(), 2): ""));
        });



        table.print();

//        System.out.printf("\r%-20s%-15s%-15s%-15s%-10s%-10s%-10s%-10s%-10s%-10s%-10s%-10s%n",
//                ConsoleColors.CYAN_BACKGROUND + "инструмент",
//                "isin",
//                "двкд",
//                "погашение",
//                "дк",
//                "цз",
//                "НКД",
//                "рк",
//                "Листинг",
//                "номинал",
//                "предл.",
//                "дп" + ConsoleColors.RESET);
//        for (BondModel bond: bonds) {
//            System.out.printf("\r%-15s", bond.getInstrumentName());
//            System.out.printf("%-15s", bond.getIsin());
//            System.out.printf("%-15s", bond.getDatePaymentCouponIncome());
//            System.out.printf("%-15s", bond.getRepayment());
//            System.out.printf("%-10s", bond.getDurationCoupon());
//            System.out.printf("%-10s", bond.getClosingPrice());
//            System.out.printf("%-10s", bond.getNKD());
//            System.out.printf("%-10s", bond.getCouponSize());
//            System.out.printf("%-10s", bond.getListing());
//            System.out.printf("%-10s", bond.getNominal());
//            System.out.printf("%-10s", bond.getProposal());
//            System.out.printf("%-10s", bond.getUntilMaturity());
//            System.out.printf("%-10s", (bond.getFeeCurrBrokerExchange() != 0? print(bond.getFeeCurrBrokerExchange(), 2): ""));
//            System.out.printf("%-10s", (bond.getPaymentToBuyBond() != 0? print(bond.getPaymentToBuyBond(), 2): ""));
//            System.out.printf("%-10s", bond.getAmountCouponsPayments());
//            System.out.printf("%-10s", (bond.getSummOfCouponsWithoutTax13() != 0? print(bond.getSummOfCouponsWithoutTax13(), 2): ""));
//            System.out.printf("%-10s%n", (bond.getSummToRepayment() != 0? print(bond.getSummToRepayment(), 2): ""));
//        }

    }

    public void getSummToRepayment(List<BondModel> bonds) {
        for (BondModel bond: bonds) {
            double result = (double) bond.getNominal() + (double) bond.getSummOfCouponsWithoutTax13();
            bond.setSummToRepayment(result);
        }
    }

    public void getSummOfCouponsWithoutTax13(List<BondModel> bonds) {
        for (BondModel bond: bonds) {
            double result = (double) bond.getListing() * (double) bond.getAmountCouponsPayments() * 0.87;
            bond.setSummOfCouponsWithoutTax13(result);
        }
    }

    public void getAmountCouponsPayments(List<BondModel> bonds) {
        for (BondModel bond: bonds) {
            double result = (double) bond.getUntilMaturity() / (double) bond.getDurationCoupon();
            bond.setAmountCouponsPayments((int) Math.ceil(result));
        }
    }

    public void getPaymentToBuy(List<BondModel> bonds) {
        for (BondModel bond: bonds) {
            double NomProp =  (bond.getNominal() * bond.getProposal()) / 100.0d;
            double result = (NomProp + bond.getNKD() + bond.getFeeCurrBrokerExchange());
            bond.setPaymentToBuyBond(result);
        }
    }

    public void getFeeCurOfBondBrokerAndExchange (List<BondModel> bonds, double feePercent) {

        feePercent = feePercent / 100.0d;

        for (BondModel bond: bonds) {
            double NomProp =  (bond.getNominal() * bond.getProposal()) / 100.0d;
            double result = (NomProp + bond.getNKD()) * feePercent;
            bond.setFeeCurrBrokerExchange(result);
        }

    }


    public String print(double number, int nums) {
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(nums);
        return  df.format(number).indexOf(',') == 0 ?
                "0" +  df.format(number):  df.format(number);
    }


    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


}
