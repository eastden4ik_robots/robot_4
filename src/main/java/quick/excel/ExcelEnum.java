package quick.excel;

public enum  ExcelEnum {

    Bonds("ОБЛИГАЦИИ"), Stocks("АКЦИИ"), Currency("ВАЛЮТА");

    private String name;

    ExcelEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
