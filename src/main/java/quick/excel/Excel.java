package quick.excel;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import quick.models.BondModel;
import ru.sviridoff.framework.logger.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Excel {

    private static XSSFSheet sheet;

    private static Logger logger = new Logger(Excel.class.getName());

    private static Excel excelInstance = null;

    private Excel(String file, ExcelEnum page) {
        try {
            String excelPage = page.getName();

            XSSFWorkbook excelWorkBook = new XSSFWorkbook(new File(file));
            sheet = excelWorkBook.getSheet(excelPage);

            if (sheet == null) {
                throw new RuntimeException("Данной страницы [" + page.getName() + "] нет.");
            }
        } catch (IOException | InvalidFormatException ex) {
            logger.error(ex.getMessage());
        }
    }

    public static Excel getInstance(String file, ExcelEnum page) {
        if (excelInstance == null) excelInstance = new Excel(file, page);
        return excelInstance;
    }

    public List<BondModel> getBonds() {
        List<BondModel> bonds = new ArrayList<>();

        int rows = sheet.getPhysicalNumberOfRows();

        for (int i = 1; i < rows; i++) {
            BondModel bond = new BondModel();
            bond.setName(sheet.getRow(i).getCell(0).getStringCellValue());
            bond.setInstrumentName(sheet.getRow(i).getCell(1).getStringCellValue());
            bond.setIsin(sheet.getRow(i).getCell(2).getStringCellValue());
            bond.setDatePaymentCouponIncome(sheet.getRow(i).getCell(3).getStringCellValue());
            bond.setRepayment(sheet.getRow(i).getCell(4).getStringCellValue());
            bond.setDurationCoupon((long) sheet.getRow(i).getCell(5).getNumericCellValue());
            bond.setClosingPrice(sheet.getRow(i).getCell(6).getNumericCellValue());
            bond.setNKD(sheet.getRow(i).getCell(7).getNumericCellValue());
            bond.setCouponSize(sheet.getRow(i).getCell(8).getNumericCellValue());
            bond.setListing((long) sheet.getRow(i).getCell(9).getNumericCellValue());
            bond.setNominal(sheet.getRow(i).getCell(10).getNumericCellValue());
            bond.setProposal(sheet.getRow(i).getCell(11).getNumericCellValue());
            bond.setUntilMaturity((int) sheet.getRow(i).getCell(12).getNumericCellValue());

            bonds.add(bond);
        }

        return bonds;
    }



}
