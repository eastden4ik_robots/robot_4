package quick.models;

import lombok.Data;

@Data
public class BondModel {

    private String name; // наименование
    private String instrumentName; // инструмент
    private String isin; // isin
    private String datePaymentCouponIncome; // дата выплаты купонного дохода
    private String repayment; // погашение
    private long durationCoupon; // длительность купона
    private double closingPrice; // цена закрытия
    private double NKD; // НКД
    private double couponSize; // Размер купона
    private long listing; // Листинг
    private double nominal; // номинал
    private double proposal; // предложение
    private int untilMaturity; // до погашения

    // Переменные заполняются по расчетам
    private double feeCurrBrokerExchange;
    private double paymentToBuyBond;
    private int amountCouponsPayments;
    private double summOfCouponsWithoutTax13;
    private double summToRepayment;


}
